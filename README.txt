
When you have untranslated content, the language switcher is showing a lined-through link to the languages, that are untranslated.
That makes it impossible for users to switch the site-language to a different one on untranslated pages.
No more! Language Switcher Settings lets you specify a fallback link for untranslated pages of all active languages.

Installation
------------

1. Copy locale_language_switcher_settings.module to your module directory and then enable on the admin modules page.
2. On admin/config/regional/language-switcher you can specify a fallback link for untranslated pages of all active languages.