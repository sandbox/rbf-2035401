<?php

/**
 * @file
 * Page callbacks for locale language switcher settings administration code
 */

/**
 * locale language switcher settings configuration form.
 *
 * @param array $form
 * @param array $form_state
 * @return array
 */
function language_switcher_settings_general_form($form, &$form_state) {

  $form = array();
  $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('Language switcher settings for untranslated pages'),
      '#description' => t('By default all untranslated language links are disabled by the locale language switcher. You can specify a fallback link for untranslated pages of all active languages.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  // get active languages
  $form_state['languages_active'] = language_list();

  // set variables for each language
  foreach ($form_state['languages_active'] as $languageid => $language) {
    $form['general']['language_switcher_settings_' . $languageid] = array(
        '#type' => 'textfield',
        '#title' => check_plain($language->name),
        '#default_value' => variable_get('language_switcher_settings_' . $languageid, ''),
        '#size' => 128,
        '#maxlength' => 255,
        '#description' => t('Enter a redirect url for language link %language. %front is the front page.', array('%language' => $language->name, '%front' => '<front>')),
    );
  }

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
      '#weight' => 5,
  );

  return $form;
}

/**
 * locale language switcher settings configuration form validate function.
 *
 * @param array $form
 * @param array $form_state
 */
function language_switcher_settings_general_form_validate($form, &$form_state) {

  // check if page exist
  foreach ($form_state['languages_active'] as $languageid => $language) {

    // input value by language
    $link_path = $form_state['input']['language_switcher_settings_' . $languageid];

    // check for node pages
    if (!trim($link_path) || !drupal_valid_path($link_path)) {
      form_set_error('language_switcher_settings_' . $languageid, t("The path '@link_path' is invalid.", array('@link_path' => $link_path)));
    }
  }
}

/**
 * locale language switcher settings configuration form submission function.
 *
 * @param array $form
 * @param array $form_state
 */
function language_switcher_settings_general_form_submit($form, &$form_state) {

  // set variables for each language
  foreach ($form_state['languages_active'] as $languageid => $language) {
    variable_set('language_switcher_settings_' . $languageid, $form_state['input']['language_switcher_settings_' . $languageid]);
  }

  drupal_set_message(t('Configuration saved'), 'status');
}